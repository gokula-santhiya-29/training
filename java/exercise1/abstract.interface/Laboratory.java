package org.qwerty;
interface Equipment{
	void availableEquipment();
}

public class Laboratory implements Equipment{
	public void availableEquipment(){
		System.out.println("pippet,funnel,pH paper");
	}
	
	public static void main(String[] args){
		Laboratory l = new Laboratory();
		l.availableEquipment();
	
	}
}
		
