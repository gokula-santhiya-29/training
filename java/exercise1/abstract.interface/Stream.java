package edu.qwerty;
interface Type{
	public String univName = "Qwerty University";
	public int univID = 001;
	public String location = "New York";	
	public void getStream();
}
class Stream implements Type{
		
	public void getStream(){
		System.out.println("Engineering,Architecture,Law");
	}

	public static void main(String[] args){
		Stream s = new Stream();
		System.out.println(Type.univName);
		s.getStream();
	}
}