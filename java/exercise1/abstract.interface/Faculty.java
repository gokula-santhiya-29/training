package learn;
abstract class TimeTable{
	public abstract void timeTable();
}
public class Faculty extends TimeTable{ 
	public void timeTable(){
		System.out.println("CN,ANT,OOAD");
	}
}
class abstraction{
	public static void main(String[] args){
		Faculty f = new Faculty();
		f.timeTable();
	}
}
