package org.qwerty;
abstract class Student{
	public String name;
	public String rollNo;
	public void getSemDetails(){
		System.out.println("The Semester is postponed due to COVID-19 and you'll get to know the date shortly.");
	}
}
public class Institute extends Student{
	public static void main(String[] args){
		Institute i = new Institute();
		i.getSemDetails();
	}
}
	
