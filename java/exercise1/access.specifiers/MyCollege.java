class College{
	public int collegeCode;
	public String collegeName;
	public int Rank;
	
	class Student{
		public String studentName;
		public int rollNo;
		public String dept;
		protected String studentMail;
		private String studentPass;
	}
}

public class MyCollege{
	public static void main(String args[]){
		College c = new College();
		College.Student s = c.new Student();
		c.collegeCode = 2707;
		c.collegeName = "XYZ";
		c.Rank = 6;
		s.studentName = "Heera";
		s.rollNo = 3;
		s.dept = "CSE";
		s.studentMail = "2707cs03@xyz.ac.in";
		System.out.println(s.dept);
	}
}