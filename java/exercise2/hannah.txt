 String hannah = "Did Hannah see bees? Hannah did.";
 
1) The value displayed by the expression hannah.length() is 32.
2) The value returned by the method call hannah.charAt(12) is e.
3) The expression referring to the letter b in the string is hannah.charAt(15).