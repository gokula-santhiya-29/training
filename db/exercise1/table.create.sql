CREATE TABLE `sys`.`emp` (
  `emp_id` INT NOT NULL,
  `emp_dept` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`emp_id`));
  
CREATE TABLE `sys`.`dept` (
  `dept_id` INT NOT NULL,
  `dept_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`dept_id`));
  
  

