INSERT INTO `sys`.`emp` (`emp_id`, `emp_dept`) VALUES ('1', 'HR');
INSERT INTO `sys`.`emp` (`emp_id`, `emp_dept`) VALUES ('2', 'Dev');
INSERT INTO `sys`.`emp` (`emp_id`, `emp_dept`) VALUES ('3', 'Operation');
INSERT INTO `sys`.`emp` (`emp_id`, `emp_dept`) VALUES ('4', 'Admin');

INSERT INTO `sys`.`dept` (`dept_id`, `dept_name`) VALUES ('101', 'HR');
INSERT INTO `sys`.`dept` (`dept_id`, `dept_name`) VALUES ('102', 'Dev');
INSERT INTO `sys`.`dept` (`dept_id`, `dept_name`) VALUES ('103', 'Operations');
INSERT INTO `sys`.`dept` (`dept_id`, `dept_name`) VALUES ('104', 'Admin');
