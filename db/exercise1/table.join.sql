SELECT * FROM dept
    JOIN emp
        ON dept.dept_id =emp.emp_id;
        
SELECT * FROM dept
    INNER JOIN emp
        ON dept.dept_id =emp.emp_id;
        
SELECT * FROM dept
    LEFT JOIN emp
        ON dept.dept_id =emp.emp_id;

SELECT * FROM dept
    RIGHT JOIN emp
        ON dept.dept_id =emp.emp_id;