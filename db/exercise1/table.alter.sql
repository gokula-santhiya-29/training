ALTER TABLE `sys`.`emp` 
ADD COLUMN `emp_name` VARCHAR(45) NOT NULL AFTER `emp_dept`,
ADD COLUMN `date_of_joining` DATE NOT NULL AFTER `emp_name`;

ALTER TABLE `sys`.`dept` 
ADD COLUMN `manager_id` INT NOT NULL AFTER `dept_name`;

