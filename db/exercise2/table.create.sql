CREATE TABLE `sys`.`employee` (
  `emp_id` INT NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NULL,
  `dob` DATE NOT NULL,
  `date_of_joining` DATE NOT NULL,
  `annual_salary` FLOAT NOT NULL,
  PRIMARY KEY (`emp_id`));
  
CREATE TABLE `sys`.`department` (
  `dept_id` INT NOT NULL,
  `dept_name` VARCHAR(45) NULL,
  `manager_id` INT NOT NULL,
  `loc` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`dept_id`));
  
ALTER TABLE `sys`.`employee` 
ADD CONSTRAINT `dept_id`
  FOREIGN KEY (`emp_id`)
  REFERENCES `sys`.`department` (`dept_id`);