INSERT INTO `sys`.`department` (`dept_id`, `dept_name`, `manager_id`, `loc`) VALUES ('1', 'ITDesk', '001', 'NorthBlock');
INSERT INTO `sys`.`department` (`dept_id`, `dept_name`, `manager_id`, `loc`) VALUES ('2', 'Finance', '002', 'SouthBlock');
INSERT INTO `sys`.`department` (`dept_id`, `dept_name`, `manager_id`, `loc`) VALUES ('3', 'Engineering', '003', 'EastBlock');
INSERT INTO `sys`.`department` (`dept_id`, `dept_name`, `manager_id`, `loc`) VALUES ('4', 'HR', '004', 'WestBlock');
INSERT INTO `sys`.`department` (`dept_id`, `dept_name`, `manager_id`, `loc`) VALUES ('5', 'Recruitment', '005', 'NorthBlock');
INSERT INTO `sys`.`department` (`dept_id`, `dept_name`, `manager_id`, `loc`) VALUES ('6', 'Facility', '006', 'SouthBlock');

ALTER TABLE `sys`.`employee` 
ADD COLUMN `dept_id` VARCHAR(45) NULL AFTER `annual_salary`;

INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1001', 'Joy', 'Carole', '1999-03-23', '2018-04-30', '80000', '1');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1002', 'Jency', 'Francis', '1999-04-30', '2018-03-04', '87000', '1');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1004', 'Adams', 'Johnson', '2000-05-12', '2019-07-30', '98000', '1');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1067', 'Sameera', 'Singh', '2000-03-11', '2020-08-03', '120000', '1');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1034', 'Maria', 'Albert', '2020-06-01', '2019-04-12', '57000', '1');

INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1056', 'Mithila', '', '2000-11-29', '2020-01-01', '50000', '2');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1045', 'Roopa', 'Kaur', '2000-11-14', '2019-02-02', '67000', '2');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1346', 'Mansi', 'Deshmukh', '2000-10-30', '2018-03-14', '45000', '2');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1029', 'Sanya', 'Kaur', '2000-08-24', '2018-03-14', '78000', '2');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1005', 'Prateeksha', 'Pandey', '2000-11-02', '2019-04-17', '90000', '2');

INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1456', 'Princy', '1998-03-13', '2916-05-10', '45000', '3');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1236', 'Mishra', '1998-06-14', '2019-04-06', '123000', '3');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1789', 'Varun', 'Reddy', '1997-10-23', '2018-04-03', '79450', '3');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1010', 'Ria', 'Khan', '1997-03-12', '2020-01-31', '30000', '3');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1008', 'Farah', 'Khan', '1988-02-17', '2000-11-29', '450000', '3');

INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1990', 'Nila', 'Chandak', '2000-12-29', '2019-05-12', '34000', '4');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1018', 'Vimal', 'Daga', '2000-11-29', '2019-04-25', '29000', '4');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1030', 'Arjun', '2000-11-14', '2019-04-15', '45000', '4');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1062', 'Renci', '2000-07-13', '2020-03-23', '87000', '4');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1031', 'Varun', 'Kumar', '1998-06-07', '2018-06-23', '90800', '4');

INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1022', 'Ajay', '2000-11-14', '2018-06-12', '87000', '5');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1033', 'Santhiya', '2000-11-29', '2019-05-12', '98699', '5');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1044', 'Santhosh', '2000-02-24', '2020-03-12', '55899', '5');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1055', 'Reena', 'Mishra', '2000-03-12', '2016-04-13', '78000', '5');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1066', 'Malathi', '1998-04-28', '2019-05-16', '78900', '5');

INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1751', 'Anushka', '200-11-29', '2018-03-05', '34900', '6');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1900', 'Kiran', '2000-11-14', '2019-03-06', '45000', '6');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1700', 'Chandan', 'Sharma', '2000-10-29', '2018-04-30', '67000', '6');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1500', 'Rila', 'Mehta', '2000-04-17', '2019-03-17', '57000', '6');
INSERT INTO `sys`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `dept_id`) VALUES ('1600', 'Varun', 'Thakur', '2000-11-19', '2020-05-05', '68900', '6');
