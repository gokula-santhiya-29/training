SELECT `college`.`code`,`college`.`name` as `collage_name`,`u`.`university_name`,`college`.`city`,`college`.`state`,`college`.`year_opened`,`d`.`dept_name`,`hod`.`name`
FROM `college`
JOIN ( SELECT `univ_code`,`university_name` FROM `university`) AS `u`
ON `college`.`univ_code` = `u`.`univ_code`
JOIN `college_department`
ON `college_department`.`college_id` = `college`.`id`
JOIN (SELECT `dept_name`,`dept_code` FROM `department` WHERE `dept_code` = 104 OR `dept_code` = 205) AS `d` 
ON `college_department`.`udept_code` = `d`.`dept_code`
JOIN (SELECT `name`,`cdept_id`FROM employee WHERE desig_id = 3) AS `h`
ON `hod`.`cdept_id` = `college_department`.`cdept_id`;