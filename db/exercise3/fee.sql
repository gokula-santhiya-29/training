SELECT college.`name`
      ,semester
      ,paid_status
      ,SUM(amount) AS `Total Amount Collected`
  FROM semester_fee
 JOIN college_department
        ON semester_fee.`cdept_id` = college_department.`cdept_id`
 JOIN college
        ON college.`id` = college_department.`college_id`
 GROUP BY college.`name`
         ,semester_fee.`semester`
         ,semester_fee.`paid_status`;
         
         
SELECT SUM(`amount`) AS `Total Fee Collected`
FROM `semester_fee`
WHERE `paid_status` = 'Paid' AND `paid_year` = '2020';