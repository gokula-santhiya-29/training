SELECT `student`.`roll_number`
       ,`student`.`name`
       ,`student`.`gender`
       ,`student`.`dob`
       ,`student`.`email`
       ,`student`.`phone`
       ,`student`.`address`
       ,`college`.`name`
       ,`department`.`dept_name`
    FROM student
        JOIN `college_department`
            ON `college_department`.`cdept_id` =`student`.`cdept_id`
        JOIN `department`
            ON `department`.`dept_code` = `college_department`.`udept_code`
        JOIN `college`
            ON `college`.`id` = `student`.`college_id`
WHERE `college`.`univ_code` = "unv1"
    AND `college`.`city` IN ("Coimbatore","Erode")
LIMIT 20;