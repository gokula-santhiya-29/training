CREATE TABLE `education`.`semester_fee` (
  `id` INT NOT NULL,
  `cdept_id` INT NOT NULL,
  `stud_id` INT NOT NULL,
  `semester` TINYINT NOT NULL,
  `amount` DOUBLE(18,2) NOT NULL DEFAULT 50000,
  `paid_year` YEAR(4) NULL DEFAULT NULL,
  `paid_status` VARCHAR(10) NOT NULL DEFAULT 'UNPAID',
  PRIMARY KEY (`id`),
  INDEX `CollegeDepartmentSemesterFeeForeignKey_idx` (`cdept_id` ASC) VISIBLE,
  INDEX `StudentSemesterFeeForeignKey_idx` (`stud_id` ASC) VISIBLE,
  CONSTRAINT `CollegeDepartmentSemesterFeeForeignKey`
	FOREIGN KEY (`cdept_id`)
    REFERENCES `education`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `StudentSemesterFeeForeignKey`
    FOREIGN KEY (`stud_id`)
    REFERENCES `education`.`student` (`id`));